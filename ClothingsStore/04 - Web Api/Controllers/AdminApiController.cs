﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Seldat.Controllers
{
    [EnableCors("*", "*", "*")]
    public class AdminApiController : ApiController
    {
        private AdminLogic logic = new AdminLogic();
        [HttpPost]
        [Route("api/login")]
        public HttpResponseMessage CheckAdmin([FromBody]AdminModel model)
        {
            model.userNmae = model.userNmae.Replace(";", " ");
            model.password = model.password.Replace(";", " ");
            bool flagAdmin;
            try
            {
                if (!ModelState.IsValid)
                    return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState.GetErrors());
                flagAdmin = logic.checkAdmin(model);
                return Request.CreateResponse(HttpStatusCode.Created, flagAdmin);
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                logic.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

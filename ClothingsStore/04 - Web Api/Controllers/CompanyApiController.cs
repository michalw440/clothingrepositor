﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Seldat.Controllers
{
    [EnableCors("*", "*", "*")]
    public class CompanyApiController : ApiController
    {
        private ClothCompanyLogic logic = new ClothCompanyLogic();
        [HttpGet]
        [Route("api/getCompanies")]
        public HttpResponseMessage GetCompanies()
        {
            try
            {
                if (!ModelState.IsValid)
                    return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState.GetErrors());
                List<ClothCompanyModel> companies = logic.GetCompanies();
                return Request.CreateResponse(HttpStatusCode.Created, companies);
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.GetUserFriendlyMessage());
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                logic.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

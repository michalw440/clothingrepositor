﻿using System.Collections.Generic;
using System.Linq;

namespace Seldat
{
    public class ClothCompanyLogic : BaseLogic
    {
        public List<ClothCompanyModel> GetCompanies()
        {
            return DB.Companies.Select(c => new ClothCompanyModel { id = c.CompanyID, name = c.CompanyName }).ToList();
        }
    }
}

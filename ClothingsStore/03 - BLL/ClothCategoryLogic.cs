﻿using System.Collections.Generic;
using System.Linq;

namespace Seldat
{
    public class ClothCategoryLogic : BaseLogic
    {
        public List<ClothCategoryModel> GetCategories()
        {
            return DB.Categories.Select(c => new ClothCategoryModel { id = c.CategoryID, name = c.CategoryName }).ToList();
        }
    }
}

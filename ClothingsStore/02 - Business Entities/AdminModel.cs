﻿using System.ComponentModel.DataAnnotations;

namespace Seldat
{
    public class AdminModel
    {
        public int id { get; set; }
        [Required(ErrorMessage = "Missing User name")]
        public string userNmae { get; set; }
        [Required(ErrorMessage = "Missing Password")]
        [RegularExpression("^.{4}$", ErrorMessage = "password must be 4 chars")]
        public string password { get; set; }
    }
}

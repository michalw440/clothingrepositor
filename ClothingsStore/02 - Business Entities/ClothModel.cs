﻿using System.ComponentModel.DataAnnotations;

namespace Seldat
{
    public class ClothModel
    {
        public int id { get; set; }
        [Required(ErrorMessage = "Missing category")]
        public ClothCategoryModel category { get; set; }
        [Required(ErrorMessage = "Missing Company")]
        public ClothCompanyModel company { get; set; }
        [Required(ErrorMessage = "Missing Type")]
        public ClothTypeModel type { get; set; }
        [Required(ErrorMessage = "Missing Price")]
        [Range(0, int.MaxValue, ErrorMessage = "price cant be negative and cant be too large")]
        public double? price { get; set; }
        public float? discount { get; set; }
        public string picture { get; set; }
    }
}
